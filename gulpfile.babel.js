
import Taskerify from 'taskerify';

Taskerify.config.sourcemaps = false;
Taskerify.config.srcPath = './src/assets';
Taskerify.config.distPath = './dist/assets';
Taskerify.config.srcViewsPath = './src';
Taskerify.config.distViewsPath = './dist';

const NODE_MODULES = './node_modules';
const SRC = Taskerify.config.srcPath;
const DIST = Taskerify.config.distPath;

const storeName = 'alisson';
const commomFiles = ['general'];
const desktopFiles = [ 'general'];
const mobileFiles = [ 'general'];

Taskerify((mix) => {

    // Common Files
    commomFiles.map((file) => {
        mix.browserify(`${SRC}/common/js/${storeName}-common-${file}.js`, `${DIST}/common/js`)
        .sass(`${SRC}/common/scss/${storeName}-common-${file}.scss`, `${DIST}/common/css`);
    });

    // Main Desktop Files
    desktopFiles.map((file) => {
        mix.browserify(`${SRC}/desktop/js/${storeName}-desktop-${file}.js`, `${DIST}/desktop/js`)
        .sass(`${SRC}/desktop/scss/${storeName}-desktop-${file}.scss`, `${DIST}/desktop/css`)
    });

    // Main Mobile Files
    mobileFiles.map((file) => {
        mix.browserify(`${SRC}/mobile/js/${storeName}-mobile-${file}.js`, `${DIST}/mobile/js`)
        .sass(`${SRC}/mobile/scss/${storeName}-mobile-${file}.scss`, `${DIST}/mobile/css`);
    });
});